# Sets up machine to be deb AND RPM repo master.
class repomaster::yum_repository
{
  $arches = [
    'x86_64',
  ]

  ensure_packages(['createrepo-c'])

  # RHEL, CentOS, Rocky, etc.
  repomaster::yum_repository::init_repos { 'enterprise linux':
    os_slug  => 'el',
    versions => [
      '7',
      '8',
      '9',
    ],
    arches   => $arches,
  }

  # Fedora
  repomaster::yum_repository::init_repos { 'fedora':
    os_slug  => 'fedora',
    versions => [
      '35',
      '36',
      '37',
      '38',
      '39',
    ],
    arches   => $arches,
  }

  # contains chronic, required by lysator_update_yum_repos_cron
  ensure_packages(['moreutils'])

  # automatically create repos for new fedora versions as they are released
  ensure_packages(['jq'])
  file { '/usr/local/bin/create_fedora_repos':
    ensure => file,
    source => "puppet:///modules/repomaster/create_fedora_repos",
    owner  => root,
    group  => root,
    mode   => '0555',
  }
  cron { 'create_fedora_repos':
    command => '/usr/local/bin/create_fedora_repos',
    user    => root,
    weekday => 6,
    hour    => 6,
    minute  => 15,
  }

  file { '/usr/local/bin/lysator_update_yum_repos_cron':
    ensure => file,
    source => "puppet:///modules/repomaster/lysator_update_yum_repos_cron",
    owner  => root,
    group  => root,
    mode   => '0555',
  }

  cron { 'lysator_update_yum_repos_cron':
    command => '/usr/local/bin/lysator_update_yum_repos_cron',
    user    => root,
    minute  => 24,
  }

  file { '/usr/local/bin/add_latest_thinlinc_client_to_repos':
    ensure => file,
    source => "puppet:///modules/repomaster/add_latest_thinlinc_client_to_repos",
    owner  => root,
    group  => root,
    mode   => '0555',
  }

  cron { 'add_latest_thinlinc_client_to_repos':
    command => '/usr/local/bin/add_latest_thinlinc_client_to_repos',
    user    => root,
    weekday => 6,
    hour    => 6,
    minute  => 15,
  }
}
