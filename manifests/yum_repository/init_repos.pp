define repomaster::yum_repository::init_repos (
  String $os_slug,
  Array[String] $versions,
  Array[String] $arches,
) {
  file { "/srv/repos/${os_slug}/":
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0775',
  }

  $versions.each |$version| {
    file { "/srv/repos/${os_slug}/${version}":
      ensure  => directory,
      owner   => root,
      group   => root,
      mode    => '0775',
      require => File["/srv/repos/${os_slug}/"],
    }
    $arches.each |$arch| {
      $repo_path = "/srv/repos/${os_slug}/${version}/${arch}"
      file { $repo_path:
        ensure  => directory,
        owner   => root,
        group   => root,
        mode    => '0775',
        require => File["/srv/repos/${os_slug}/${version}"],
      }
      ~> exec { "/usr/bin/createrepo_c ${repo_path}":
        refreshonly => true,
      }
    }
  }
}
