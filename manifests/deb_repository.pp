class repomaster::deb_repository
{
  ensure_packages(['reprepro', 'nginx', 'gnupg'])

  file { '/etc/motd':
    ensure  => file,
    content => @(EOF)
    Repositories in /srv/repos
    Mer information https://datorhandbok.lysator.liu.se/index.php/Rootmanual:Repository
    | EOF
  }

  file {
    [ '/srv/repos/ubuntu/conf/notify-changes.sh', '/srv/repos/debian/conf/notify-changes.sh' ]:
      ensure => present,
      source => "puppet:///modules/repomaster/reprepro/notify-changes.sh",
      group  => 'root',
      owner  => 'root',
      mode   => '0755',
  }

  ### Ubuntu
  file {
    [
      '/srv/repos', '/srv/repos/ubuntu', '/srv/repos/ubuntu/conf',
      '/srv/repos/ubuntu/db', '/srv/repos/ubuntu/incoming',
    ]:
      ensure => 'directory',
      group  => 'root',
      owner  => 'root',
      mode   => '0775',
      before => [ File['ubuntu-options-repo'], File['ubuntu-distributions-repo']],
  }

  file {
    'ubuntu-distributions-repo':
      ensure => 'file',
      source => "puppet:///modules/repomaster/reprepro/ubuntu-distributions",
      path   => '/srv/repos/ubuntu/conf/distributions',

      group  => 'root',
      owner  => 'root',
      mode   => '0775',
  }

  file {
    'ubuntu-options-repo':
      ensure => 'file',
      source => "puppet:///modules/repomaster/reprepro/ubuntu-options",
      path   => '/srv/repos/ubuntu/conf/options',

      group  => 'root',
      owner  => 'root',
      mode   => '0775',
  }

  file {
    '/srv/repos/ubuntu/conf/override.precise':
      ensure => 'present',
      group  => 'root',
      owner  => 'root',
      mode   => '0775',
  }

  file {
    '/srv/repos/ubuntu/conf/override.trusty':
      ensure => 'present',
      group  => 'root',
      owner  => 'root',
      mode   => '0775',
  }

  file {
    '/srv/repos/ubuntu/conf/override.bionic':
      ensure => 'present',
      group  => 'root',
      owner  => 'root',
      mode   => '0775',
  }

  ### Debian
  file {
    [
      '/srv/repos/debian', '/srv/repos/debian/conf', '/srv/repos/debian/db',
      '/srv/repos/debian/incoming',
    ]:
      ensure => 'directory',
      group  => 'root',
      owner  => 'root',
      mode   => '0775',
      before => [ File['debian-options-repo'], File['debian-distributions-repo']],
  }

  file {
    'debian-distributions-repo':
      ensure => 'file',
      source => "puppet:///modules/repomaster/reprepro/debian-distributions",
      path   => '/srv/repos/debian/conf/distributions',

      group  => 'root',
      owner  => 'root',
      mode   => '0775',
  }

  file {
    'debian-options-repo':
      ensure => 'file',
      source => "puppet:///modules/repomaster/reprepro/debian-options",
      path   => '/srv/repos/debian/conf/options',

      group  => 'root',
      owner  => 'root',
      mode   => '0775',
  }

  file {
    '/srv/repos/debian/conf/override.wheezy':
      ensure => 'present',
      group  => 'root',
      owner  => 'root',
      mode   => '0775',
  }

  ### nginx
  file { '/etc/nginx/sites-enabled/default':
    ensure  => absent,
    notify  => Service['nginx'],
  }

  file {
    'repo-nginx-conf':
      ensure  => 'file',
      content => template("repomaster/nginx/repo"),
      path    => '/etc/nginx/sites-enabled/repo',

      group   => 'root',
      owner   => 'root',
      mode    => '0644',

      notify  => Service['nginx'],
  }

  service { 'nginx':
    ensure => running,
    enable => true,
  }

  file_line { 'gpg-digest-algo-preference':
    path => '/root/.gnupg/gpg.conf',
    line => 'personal-digest-preferences SHA512',
  }
}
