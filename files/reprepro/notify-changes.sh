#!/bin/sh

ACTION="$1"
DISTRIBUTION="$2"
PKGTYPE="$3"
COMPONENT="$4"
ARCH="$5"
PKGNAME="$6"

if [ "$ACTION" = "add" ]
then
	ACTION_PAST="added"
	NEWVERSION="$7"
	VERSIONDESC="${NEWVERSION}"
	shift 8
elif [ "$ACTION" = "remove" ]
then
	ACTION_PAST="removed"
	OLDVERSION="$7"
	VERSIONDESC="${OLDVERSION}"
	shift 8
elif [ "$ACTION" = "replace" ]
then
	ACTION_PAST="replaced"
	NEWVERSION="$7"
	OLDVERSION="$8"
	VERSIONDESC="${OLDVERSION} -> ${NEWVERSION}"
	shift 9
fi


sendmail -t >> /tmp/notify-changes-test.log <<EOF
From: <root@repomaster.lysator.liu.se>
To: <14606@kom.lysator.liu.se>
Subject: Repository updated: ${PKGNAME} ${ACTION_PAST}

Repository:      ${REPREPRO_BASE_DIR}

Action:          ${ACTION}
Distribution:    ${DISTRIBUTION}
Component:       ${COMPONENT}
Architechture:   ${ARCH}
Package:         ${PKGNAME}
Version:         ${VERSIONDESC}

Affected files:
$(printf "%s\n" ${@})

EOF
